package grifts

import (
	"github.com/gobuffalo/buffalo"
	"gitlab.com/barkerd427/ytty/actions"
)

func init() {
	buffalo.Grifts(actions.App())
}
