package main

import (
	"log"

	"gitlab.com/barkerd427/ytty/actions"
)

func main() {
	app := actions.App()
	if err := app.Serve(); err != nil {
		log.Fatal(err)
	}
}
